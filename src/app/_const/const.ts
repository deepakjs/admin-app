export const SITE_CONFIG = {
    BASE: 'https://adminapp321.herokuapp.com/',
    PROXY: 'https://cors-anywhere.herokuapp.com/',
    BASE_ORIGIN: window.location.origin
}