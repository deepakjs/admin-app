import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { HttpModule, BrowserXhr } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgProgressModule,  NgProgressBrowserXhr, NgProgressInterceptor } from 'ngx-progressbar';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgApexchartsModule } from 'ng-apexcharts';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular5-social-login";

import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from "./guard/auth.guard";
import { RoleGuardService } from "./guard/role.guard";

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Requestterceptor } from './_config/interceptor/request.interceptor';
import { ResponseInterceptor } from './_config/interceptor/response.interceptor';

import { LoginService } from './shared/services/login.service';
import { UserService } from './shared/services/user.service';
import { GetRandomService } from './shared/services/get-random.service';
import { NotauthorizedComponent } from './pages/notauthorized/notauthorized.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { ChartComponent } from './shared/components/chart/chart.component';
import { ResetpasswordComponent } from './pages/resetpassword/resetpassword.component';
import { ChartInfoWidgetComponent } from './shared/components/chart-info-widget/chart-info-widget.component';
import { PieChartComponent } from './shared/components/pie-chart/pie-chart.component';

// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('Your-Facebook-app-id')
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('657389663045-qvlvfckrn35b9477a1qeckrcdn8safdd.apps.googleusercontent.com')
      },
    ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NotauthorizedComponent,
    HeaderComponent,
    SidebarComponent,
    ChartComponent,
    ResetpasswordComponent,
    ChartInfoWidgetComponent,
    PieChartComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    SocialLoginModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(
      ROUTES,
      { enableTracing: true } // <-- debugging purposes only
    ),
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    HighchartsChartModule,
    NgProgressModule,
    NgApexchartsModule,
    FormsModule
  ],
  providers: [
    AuthGuard,
    RoleGuardService,
    LoginService,
    UserService,
    GetRandomService,
    NgbActiveModal,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Requestterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true
    },
    { provide: BrowserXhr, useClass: NgProgressBrowserXhr },
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
