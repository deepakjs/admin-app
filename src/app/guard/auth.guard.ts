import { Injectable } from '@angular/core';
import { Router, CanLoad, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { LoginService } from '../shared/services/login.service';

/**
 * Authentication Guard to activate / deactivate the route.
 * User will be redirected to login page, if not loged in when trying to access any page.
 *
 * @export
 * @class AuthGuard
 * @implements {CanLoad}
 * @implements {CanActivate}
 * @version 1.0
 * @author
 */
@Injectable()
export class AuthGuard implements CanLoad, CanActivate {
  /**
   * Creates an instance of AuthGuard.
   * @param {AuthenticationService} authService AuthenticationService Instance
   * @param {Router} router Router Instance
   */
  constructor(private loginService: LoginService, private router: Router) {}

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin();
  }

  checkLogin(): boolean {
    if (this.loginService.isLoggedIn()) {
      return true;
    }
    
    const nextRoute =  '/login';
    // Navigate to the login page with extras
    this.router.navigate([nextRoute]);
    return false;
  }
}
