import { SITE_CONFIG } from '../_const/const';
export const API_URL = {
  LOGIN: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}api/login`,
  SIGNUP: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}api/signup`,
  SOCIAL_SIGNUP: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}api/socialSignup`,
  GET_USERS: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}secureApi/user`,
  PASSWORD_RESET_REQUEST: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}api/changepassword`,
  PASSWORD_RESET_SAVE: `${ SITE_CONFIG.PROXY}${ SITE_CONFIG.BASE}api/savepassword`
};
