import { Injectable, Injector } from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpErrorResponse,
    HttpHeaders
} from '@angular/common/http';
import { LoginService } from '../../shared/services/login.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class Requestterceptor implements HttpInterceptor {
  constructor(
      public auth: LoginService,
      public router: Router,
      private injector: Injector
    ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.auth.getToken();
    console.log(typeof token);
    console.log(token);
    if(token !== 'undefined' && token !== '') {
      console.log('why are you here')
      request = request.clone({
          setHeaders: {
              token: `${token}`
          }
      });
    }
    return next.handle(request);
    
  }
}
