import { Injectable, Injector } from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpErrorResponse,
    HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { NotificationsService } from 'angular2-notifications';

interface ServerResponse {
  responseCode: number;
  responseObject: any;
  success: boolean;
  message: string;
}


@Injectable()

export class ResponseInterceptor implements HttpInterceptor {
    constructor(private notificationsService: NotificationsService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
          .handle(req)
          .map((event: HttpEvent<any>) => {
            // Parse HttpResponse for object mapping and error handling
            if (req.method === 'POST') {
              if (event instanceof HttpResponse) {
                const response = event.body;
                // if response contains responseCode key, process and extract responseObject
                if (response.success === false) {
                  if (response.message) {
                    this.notificationsService.error(response.message);
                  }
                } else {
                  if (response.message) {
                    this.notificationsService.success(response.message);
                  }
                    this.notificationsService.success(response.message);
                }
              }
            }
            return event;
          })
          .catch((err: any, caught: Observable<HttpEvent<any>>) => {
            let httpErrorResponse: HttpErrorResponse;
            if (err instanceof HttpErrorResponse) {
              httpErrorResponse = err;
            }
            // this.handleError(httpErrorResponse);
            return Observable.throw(httpErrorResponse);
          });
      }
}
