import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../shared/services/login.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  resetPassword;
  paramId;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
    this.paramId = this.route.snapshot.paramMap.get('id');
    if(!this.paramId) {
      this.router.navigate(['/login']);
    }
    this.resetPassword = new FormGroup({
      newpassword: new FormControl('', Validators.required),
      confirmpassword: new FormControl('', Validators.required)
    });
  }

  resetPasswordSave() {
    const obj = {
      password: this.resetPassword.value.newpassword,
      key: this.paramId
    };
    this.loginService.chagnePassword(obj).subscribe( (result: any) => {
      if(result.success) {
        this.router.navigate(['/login']);
      }
    });
  }
}
