import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormControl, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { SITE_CONFIG } from '../../_const/const'

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';

import { LoginService } from '../../shared/services/login.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm;
  passwordRequestForm;
  closeResult: string;
  modalRef;
  private loggedIn: boolean;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private socialAuthService: AuthService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    if (this.loginService.isLoggedIn()) {
      console.log('is logedin')
      this.router.navigate(['/admindashboard']);
    }
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
    const URL = `${SITE_CONFIG.BASE_ORIGIN}/resetpassword/`;
    this.passwordRequestForm = new FormGroup({
      email: new FormControl('', Validators.required),
      url: new FormControl(URL)
    })
  }
  login() {
    this.loginService.getLogin(this.loginForm.value).subscribe( response => {
      this.router.navigate(['/admindashboard']);
    });
  }
  socialSignup(data) {
    const userData = {
      username: data.name.split(' ')[0],
      email:data.email,
      social_login_key:data.id,
      role_id: 2
    }
    this.loginService.getSignupSocial(userData).subscribe( response => {
      this.router.navigate(['/admindashboard']);
    });
  }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      console.log(GoogleLoginProvider.PROVIDER_ID);
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        this.socialSignup(userData);
            
      }
    );
  }
  open(content) {
    this.modalRef = this.modalService.open(content);
  }
  close(event) {
    if(event === 'save') {
      console.log(this.passwordRequestForm.value);
      this.loginService.sendForgotPasswordRequest(this.passwordRequestForm.value)
      .subscribe( res => {
        console.log(res);
      })
    }
    this.modalRef.close();
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
