export { LoginComponent } from './login/login.component';
export { DashboardComponent } from './dashboard/dashboard.component';
export { NotauthorizedComponent } from './notauthorized/notauthorized.component';
export { ResetpasswordComponent } from './resetpassword/resetpassword.component';
