import { Component, OnInit, ViewEncapsulation, ViewChildren} from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { GetRandomService } from '../../shared/services/get-random.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  widgetData;
  userList;
  stats;
  dataList;
  from;
  to;
  from_one;
  to_one;
  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;
  activeTop = 'share';
  activeSession = 'hour';

  rondChart = [
      [
        ['Mobile', 44.2],
        ['Desktop', 32],
        ['Tablet', 23.1]
      ],
      [
        ['Mobile', 80],
        ['Desktop', 10],
        ['Tablet', 10]
      ],
      [
        ['Mobile', 70],
        ['Desktop', 20],
        ['Tablet', 10]
      ],
      [
        ['Mobile', 60],
        ['Desktop', 35],
        ['Tablet', 5]
      ]
  ];
  deviceView = [];
  @ViewChildren ('widgt') widget;
  constructor(
    private userServcie: UserService,
    private getRandom: GetRandomService
  ) { }

  ngOnInit() {
    this.deviceView = this.rondChart[0]
    this.userServcie.getUsers().subscribe( list => {
      this.userList = list;
    });
    this.widgetData = [
      {
        stats: this.getRandom.getRandArray(8),
        color: '#68b3fc',
        fillColor: '#e5f1fe',
        figur: '280390',
        status:'up',
        statusNumber:'17.4',
        label: 'IMPORTS'
      },
      {
        stats: this.getRandom.getRandArray(8),
        color: '#abe9cb',
        fillColor: '#e8faf1',
        figur: '1294',
        status:'down',
        statusNumber:'17.4',
        label: 'SPENTS'
      },
      {
        stats: this.getRandom.getRandArray(8),
        color: '#fde197',
        fillColor: '#fff7e5',
        figur: '8391',
        status:'middle',
        statusNumber:'17.4',
        label: 'CLICKS'
      },
      {
        stats: this.getRandom.getRandArray(8),
        color: '#f299af',
        fillColor: '#fcecef',
        figur: '6.43',
        status:'up',
        statusNumber:'17.4',
        label: 'EXPORTS'
      }
    ]
    this.stats = this.getRandom.getRandArray(8);
    this.dataList = [
      {
        id: 1,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'John Dow',
        campaign: 527,
        status: 'pending',
        total: '$20000'
      },
      {
        id: 2,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Deepak',
        campaign: 200,
        status: 'pending',
        total: '$23000'
      },
      {
        id: 3,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Ajay sharma',
        campaign: 57,
        status: 'pending',
        total: '$2000'
      },
      {
        id: 4,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Vinay',
        campaign: 52,
        status: 'pending',
        total: '$200'
      },
      {
        id: 5,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Milan Sharma',
        campaign: 5,
        status: 'pending',
        total: '$20'
      },
      {
        id: 6,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Manan',
        campaign: 51,
        status: 'pending',
        total: '$2200'
      },
      {
        id: 7,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Atit',
        campaign: 52,
        status: 'pending',
        total: '$200'
      },
      {
        id: 8,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Json',
        campaign: 87,
        status: 'pending',
        total: '$8709'
      },
      {
        id: 9,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Wiliam',
        campaign: 988,
        status: 'pending',
        total: '$9879'
      },
      {
        id: 10,
        date: '22 Feb 2018',
        time: '12:00 PM',
        customer: 'Dony Yang',
        campaign: 98,
        status: 'pending',
        total: '$75676'
      }
    ]
  }
  changeTopWidgets() {
    // this.stats = this.getRandom.getRandArray(8);
    
    this.widgetData.map(widget => {
      let wid = widget;
      wid.stats = this.getRandom.getRandArray(8);
      wid.figur = this.getRandom.getFigure(6);
      wid.statusNumber = this.getRandom.getFigure(2);
      console.log(wid)
      widget = Object.assign({}, wid);
    });
    setTimeout(()=>{
      this.widget._results.map(chartWidget => {
        chartWidget.changeChart();
      });
    },100)
  }
  changeSession() {
    const randAr = this.getRandom.getRandArray(14);
    this.getRandom.triggerRandom(randAr);
  }
  changeDevice(e) {
    this.deviceView = this.rondChart[e.value]
    this.getRandom.triggerRoundChart(this.rondChart[e.value]);
  }
  setActiveTop(top) {
    this.activeTop = top
  }
  setActiveTime(time) {
    this.activeSession = time
  }
}
