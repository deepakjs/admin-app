import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-notauthorized',
  templateUrl: './notauthorized.component.html',
  styleUrls: ['./notauthorized.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotauthorizedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
