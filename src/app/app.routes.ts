import { Routes } from '@angular/router';
import { 
  LoginComponent,
  DashboardComponent,
  NotauthorizedComponent,
  ResetpasswordComponent
} from './pages';
import { AuthGuard } from './guard/auth.guard';
import { 
  RoleGuardService as RoleGuard 
} from './guard/role.guard';
export const ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admindashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: { 
      expectedRole: 1,
      defaultRoute: 'userdashboard'
    } 
  },
  {
    path: 'userdashboard',
    component: NotauthorizedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'resetpassword/:id',
    component: ResetpasswordComponent,
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];