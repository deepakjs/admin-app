import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../_config'

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }
  getUsers() {
    return this.http.get(API_URL.GET_USERS)
  }
}
