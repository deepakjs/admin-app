import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { API_URL } from '../../_config'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }
  getLogin(loginInfo: any ): Observable<any> {
    return this.http.post(API_URL.LOGIN, loginInfo).map( res => this.setSessionData(res));
  }
  setSessionData(data) {
    localStorage.setItem('access_token', data.token);
    localStorage.setItem('current_user', JSON.stringify(data.data));
    return data.data;
  }
  isLoggedIn(): boolean {
    const token: String = this.getToken();
    return token && token.length > 0;
  }
  getToken() {
    const token = localStorage.getItem('access_token');
    return token != null ? token : '';
  }
  getUser() {
    const user = localStorage.getItem('current_user');
    return (user !== undefined) ? JSON.parse(user) : null;
  }
  getSignupSocial(data: any): Observable<any> {
    return this.http.post(API_URL.SOCIAL_SIGNUP, data).map( res => {
      console.log(res);
      this.setSessionData(res)
    });
  }
  logOut() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('current_user',);
  }
  sendForgotPasswordRequest(res) {
    return this.http.post(API_URL.PASSWORD_RESET_REQUEST, res);
  }
  chagnePassword(obj) {
    return this.http.post(API_URL.PASSWORD_RESET_SAVE, obj);
  }
}
