import { TestBed, inject } from '@angular/core/testing';

import { GetRandomService } from './get-random.service';

describe('GetRandomService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetRandomService]
    });
  });

  it('should be created', inject([GetRandomService], (service: GetRandomService) => {
    expect(service).toBeTruthy();
  }));
});
