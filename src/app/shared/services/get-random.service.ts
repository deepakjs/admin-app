import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GetRandomService {
  randomeTrigger = new BehaviorSubject<any>(null);
  randomeTriggerListner = this.randomeTrigger.asObservable();

  roundchartTrigger = new BehaviorSubject<any>(null);
  roundchartListner = this.roundchartTrigger.asObservable();
  constructor() { }
  getRandArray(length) {
    let ar = [];
    for(let i = 0; i < length; i++ ) {
      ar.push(Math.floor((Math.random() * 100) + 1));
    }
    return ar;
  }
  triggerRandom(obj){
    this.randomeTrigger.next(obj);
  }
  triggerRoundChart(chartData) {
    this.roundchartTrigger.next(chartData);
  }
  getFigure(range) {
    return Math.floor((Math.random() * Math.pow(10, range)) + 1);
  }
}
