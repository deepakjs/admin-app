import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-chart-info-widget',
  templateUrl: './chart-info-widget.component.html',
  styleUrls: ['./chart-info-widget.component.scss']
})
export class ChartInfoWidgetComponent implements OnInit {
  @ViewChild('highch') highch;
  @Input('color') color: String;
  @Input('stats') stats: any;
  @Input('fillColor') fillColor: String;
  @Input('figur') figur: String;
  @Input('status') status: String;
  @Input('statusNumber') statusNumber: String;
  @Input('label') label: String;
  data = [{
    data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
  }];
  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartOptions: Highcharts.Options; // required
  chartCallback: Highcharts.ChartCallbackFunction = (chart) => { } // optional function, defaults to null
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false
  constructor() { }

  ngOnInit() {
    
    this.chartOptions = {
      title: {
        text: ''
      },
      xAxis: {
        title: {
          text:'',
        },
        gridLineWidth: 0, 
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text:'',
        },
        gridLineWidth: 0,
        showEmpty: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          dataLabels:{
            enabled: false
          }
        }
      },
      chart: {
        height: 120,
        width: 290,
        spacing: [0,0,0,0],
        marginLeft: -100,
        marginBottom: 0,
        events: {
          load: ()=> {

          }
        }
      },
      series: [{
        type: 'areaspline',
        color: `${this.color}`,
        fillColor: `${this.fillColor}`,
        data: this.stats
      }]
    };
  }
  public changeChart() {
    this.highch.chart.series[0].setData(this.stats);
  }
}
