import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartInfoWidgetComponent } from './chart-info-widget.component';

describe('ChartInfoWidgetComponent', () => {
  let component: ChartInfoWidgetComponent;
  let fixture: ComponentFixture<ChartInfoWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartInfoWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartInfoWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
