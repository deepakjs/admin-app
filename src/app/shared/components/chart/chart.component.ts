import { Component, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
import { GetRandomService } from '../../services/get-random.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @ViewChild('highch') highch;
  data = [{
    data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
  }];
  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartOptions: Highcharts.Options; // required
  chartCallback: Highcharts.ChartCallbackFunction = (chart) => { } // optional function, defaults to null
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false
  constructor(public getRandomService: GetRandomService) { }

  ngOnInit() {
    this.getRandomService.randomeTriggerListner.subscribe(randVal => {
      console.log(randVal);
      if(randVal) {
        this.highch.chart.series[0].setData(randVal);
      }
    });
    this.chartOptions = {
      title: {
        text: ''
      },
      xAxis: {
        title: {
          text:'',
        },
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        
      },
      plotOptions: {
        series: {
          allowPointSelect: true
        }
      },
      chart: {
        spacing: [10,0,0,-15]
      },
      series: [{
        type: 'areaspline',
        data: [0, 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 0]
      }]
    };
  }

}
