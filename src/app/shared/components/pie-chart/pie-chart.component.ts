import { Component, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
import { GetRandomService } from '../../services/get-random.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  @ViewChild('highch') highch;
  data = [{
    data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
  }];
  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartOptions: Highcharts.Options; // required
  chartCallback: Highcharts.ChartCallbackFunction = (chart) => { } // optional function, defaults to null
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false
  constructor(private getRandomService: GetRandomService) { }

  ngOnInit() {
    this.getRandomService.roundchartListner.subscribe(randVal => {
      if(randVal) {
        this.highch.chart.series[0].setData(randVal);
      }
    })
    this.chartOptions = {
      title: {
        text: ''
      },
      xAxis: {
        title: {
          text:'',
        },
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        
      },
      plotOptions: {
        pie: {
          borderColor: 'transparent',
          innerSize: '80%',
          colors:['#1c88fb', '#80bcfc', '#b3d7fd'],
        },
        series:{
          dataLabels:{
            enabled: false
          }
        }
      },
      chart: {
        spacing: [0,0,0,0],
        width: 200,
        height: 200
      },
      series: [{
        type: 'pie',
        data: [
            ['Mobile', 44.2],
            ['Desktop', 32],
            ['Tablet', 23.1]
        ]
      }]
    };
  }

}
